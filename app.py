#!/usr/bin/env python3
from flask import Flask, abort, request
import json
from optimizer import optimize
import logging

app = Flask(__name__)


@app.route("/", methods=['GET', 'POST'])
def hello():
    if not request.json:
        return "It works!"
    j = request.json
    if sum([1 for a in ['width', 'length', 'n_equal', 'n_min', 'n_max', 'max_width', 'nguillotine', 'cost'] if a in j]) == 8:
        try:
            result = optimize(j['width'], j['length'], j['n_equal'], j['n_min'],
                              j['n_max'], j['max_width'], j['nguillotine'], j['cost'])
        except:
            logging.exception('Unable to optimize input data.')
            result = {
                'message': "Unexpected error.",
                'status': 'FAILED'
            }
        return json.dumps(result)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
