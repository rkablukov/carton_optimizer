import itertools
import numpy as np
from scipy.optimize import linprog

def optimize(width, length, number_equal, number_min, number_max, max_width=1400, nguillotine=999, cost=None):
    if cost is None:
        cost = np.ones(len(width))

    # Преобразуем входящие массивы к np.array
    width = np.array(width)
    length = np.array(length)
    number_equal = np.array(number_equal)
    number_min = np.array(number_min)
    number_max = np.array(number_max)
    cost = np.array(cost).astype(np.float)

    # число разных элементов
    N = len(width)
    indexes = range(N)

    def get_combinations(current_combination, iwzip, residual_width):
        '''
        Функция генерирует все возможные комбинации изделий.
        Суммарная ширина каждой комбинации должна быть меньше residual_width
        :param current_combination: [] - начальная комбинация - пустой массив
        :param iwzip: zip(indexes, width) - склейка индексов и ширины
        :param residual_width: Максимальная ширина комбинации.
        :return: Массив комбинаций
        '''
        result = []
        additions = []
        curindex = -1 if len(current_combination) == 0 else current_combination[-1]
        iwzip = [x for x in iwzip if x[1] <= residual_width and x[0] >= curindex]
        for x in iwzip:
            cc = current_combination + [x[0]]
            # cc.sort()
            result.append(cc)
            additions += get_combinations(cc, iwzip, residual_width - x[1])

        return result + additions

    def get_guillotine_factors(x, L, W):
        '''
        Функция возвращает адаптированные под количество гильотин
        коэффициенты уменьшения длин и дополнительные отходы
        :param x: комбинация
        :param L: длины элементов комбинации
        :param W: ширины элементов комбинации
        :return:
        '''
        # сортируем по длине
        sorted_zip = sorted(zip(L, x, W))
        x2 = np.array([a for _, a, _ in sorted_zip])
        L2 = np.array([a for a, _, _ in sorted_zip])
        W2 = np.array([a for _, _, a in sorted_zip])
        # создаём массивы доп. потерь и коэфиициентов уменьшения длин
        wastes2 = np.zeros(len(x))
        k2 = np.ones(len(x))
        while len(np.unique(L2)) > nguillotine:
            diffs = L2[1:] - L2[:-1]
            diffs[diffs == 0] = max(diffs) + 1  # исключим нули
            # mindiff = min(diffs)
            minindex = np.argmin(diffs)
            k2[minindex] *= L2[minindex] / L2[minindex + 1]
            wastes2[minindex] += (L2[minindex + 1] / L2[minindex] - 1) * W2[minindex]
            L2[minindex] = L2[minindex + 1]
        # сортируем назад по возрастанию
        sorted_zip = sorted(zip(x2, k2, wastes2))
        k2 = np.array([a for _, a, _ in sorted_zip])
        wastes2 = np.array([a for _, _, a in sorted_zip])
        return k2, sum(wastes2)

    # определяем все возможные комбинации,
    combinations = []
    # коэффициенты уменьшения количества под гильотины,
    factors = []
    # и отходы на миллиметр длины по каждой комбинации
    wastes = []
    
    # В списке изделий возможно появление тех, которые изготавливать не нужно.
    # Т.е. number_equal, number_min и number_max равны нулю
    zz = list(zip(indexes, width))
    zz = [x for x in zz if (number_equal[x[0]] + number_min[x[0]] + number_min[x[0]] > 0) ]
    #filtred_indexes = [x[0] for x in zz]

    for x in get_combinations([], zz, max_width):
        # количество изделий в линии должно быть не больше 4-х
        if len(x) > 4:
            continue
        W = np.array([width[i] for i in x])
        L = np.array([length[i] for i in x])
        C = np.array([cost[i] for i in x])
        # максимальная себестоимость
        max_cost = max(C)
        # фиксим нужную настройку
        combinations.append(x)
        k, add_waste = get_guillotine_factors(x, L, W)
        # в отходах учитываем себестоимость продукции и
        # добавочную стоимость от ограничения по
        # количеству гильотин
        waste = max_width - sum(W * C / max_cost) + add_waste
        wastes.append(waste)
        factors.append(k)

    # Вычислим коэффициенты матрицы ограничений
    A_eq = None  # Коэффициенты условий =
    A_ub = None  # Коэффициенты условий <=
    A = None  # Коэффициенты общие
    b_eq = np.array([])
    b_ub = np.array([])

    for i in range(N):
    #for i in filtred_indexes:
        A_line = np.array([np.zeros(len(combinations))])
        for j in range(len(combinations)):
            x = combinations[j]
            try:
                k = factors[j][x.index(i)]
            except:
                k = 1
            A_line[0, j] = k * x.count(i) / length[i]
        A = np.append(A, A_line, 0) if A is not None else A_line
        # Equal conditions
        if number_equal[i] > 0:
            A_eq = np.append(A_eq, A_line, 0) if A_eq is not None else A_line
            b_eq = np.append(b_eq, number_equal[i])
        # Another variant of equal conditions
        if number_min[i]==number_max[i] and number_min[i] > 0:
            A_eq = np.append(A_eq, A_line, 0) if A_eq is not None else A_line
            b_eq = np.append(b_eq, number_min[i])
        else:
            # Min conditions
            if number_min[i] > 0:
                A_ub = np.append(A_ub, -A_line, 0) if A_ub is not None else -A_line
                b_ub = np.append(b_ub, -number_min[i])  # with minus
            # Max conditions
            if number_max[i] > 0:
                A_ub = np.append(A_ub, A_line, 0) if A_ub is not None else A_line
                b_ub = np.append(b_ub, number_max[i]+5)  # without minus

    result = linprog(wastes, A_eq=A_eq, b_eq=b_eq, A_ub=A_ub, b_ub=b_ub, options=dict(bland=True, maxiter=10000))
    #result = linprog(wastes, A_eq=A_eq, b_eq=b_eq, A_ub=A_ub, b_ub=b_ub, method='interior-point', options=dict(maxiter=10000, presolve=True))

    if isinstance(result.x, np.ndarray):
        return {
            'combinations': combinations, 
            'lengths': result.x.tolist(), 
            'A': A.tolist() if A is not None else [],
            'nit': result.nit, 
            'success': result.success, 
            'status': 'OK'
        }
    else:
        return {'status': 'FAILED', 'message': result.message}
        